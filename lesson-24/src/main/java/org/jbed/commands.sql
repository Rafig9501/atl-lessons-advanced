CREATE TABLE IF NOT EXISTS PHONES
(
    ID          INT NOT NULL,
    PHONE_VALUE VARCHAR(55),
    CONSTRAINT PHONES_PK PRIMARY KEY (ID)
    );

CREATE TABLE IF NOT EXISTS PERSONS
(
    ID           INT NOT NULL,
    P_NAME       VARCHAR(55),
    EMAIL        VARCHAR(100),
    PHONE_NUMBER INT,
    CONSTRAINT PERSON_ID_PK PRIMARY KEY (ID),
    CONSTRAINT PERSONS_PHONE_FK FOREIGN KEY (PHONE_NUMBER) REFERENCES PHONES (ID)
    );

ALTER TABLE PERSONS
    ADD COLUMN IF NOT EXISTS SURNAME VARCHAR(255);

ALTER TABLE PERSONS
DROP COLUMN IF EXISTS SURNAME;

----------------------------------------------------------------

INSERT INTO PHONES(ID, PHONE_VALUE)
VALUES (1, '+99455555555');

INSERT INTO PERSONS(ID, P_NAME, EMAIL, PHONE_NUMBER)
VALUES (1, 'John', 'john@gmail.com', 1);

SELECT ID, PHONE_NUMBER
FROM PERSONS
WHERE ID = 1;


----------------------------------------------------------------


CREATE VIEW PERSONS_PHONE_ AS
SELECT *
FROM PERSONS
WHERE ID = 1
  AND PHONE_NUMBER = 1;

SELECT *
FROM PERSONS_PHONE_;

----------------------------------------------------------------

CREATE SCHEMA new_schema;

DROP SCHEMA IF EXISTS new_schema;

CREATE TABLE book
(
    ID        INT NOT NULL,
    BOOK_NAME VARCHAR(64),
    CONSTRAINT BOOK_ID PRIMARY KEY (ID)
);

CREATE TABLE AUTHOR
(
    ID          INT NOT NULL,
    AUTHOR_NAME VARCHAR(64),
    CONSTRAINT AUTHOR_ID PRIMARY KEY (ID)
);

CREATE TABLE BOOK_AUTHOR
(
    book_id   INT REFERENCES book (ID),
    author_id INT REFERENCES author (ID)
);

----------------------------------------------------------------

INSERT INTO BOOK_AUTHOR(book_id, author_id)
VALUES (3, 5);

SELECT *
FROM BOOK_AUTHOR
WHERE author_id = 5
  AND book_id = 3;

ALTER TABLE book
    ADD COLUMN book_page_numbers INT;

INSERT INTO AUTHOR(ID, AUTHOR_NAME)
VALUES (1, 'author1');
INSERT INTO AUTHOR(ID, AUTHOR_NAME)
VALUES (2, 'author2');
INSERT INTO AUTHOR(ID, AUTHOR_NAME)
VALUES (3, 'author3');
INSERT INTO AUTHOR(ID, AUTHOR_NAME)
VALUES (4, 'author4');
INSERT INTO AUTHOR(ID, AUTHOR_NAME)
VALUES (5, 'author5');
INSERT INTO AUTHOR(ID, AUTHOR_NAME)
VALUES (6, 'author6');

INSERT INTO book (ID, BOOK_NAME)
VALUES (1, 'book1');
INSERT INTO book (ID, BOOK_NAME)
VALUES (2, 'book1');
INSERT INTO book (ID, BOOK_NAME)
VALUES (3, 'book3');
INSERT INTO book (ID, BOOK_NAME)
VALUES (4, 'book4');
INSERT INTO book (ID, BOOK_NAME)
VALUES (5, 'book5');
INSERT INTO book (ID, BOOK_NAME)
VALUES (6, 'book6');

----------------------------------------------------------------

SELECT *
FROM book
WHERE ID = 2
  AND (BOOK_NAME = 'book4' OR book.book_page_numbers = 63);

----------------------------------------------------------------

SELECT book_page_numbers * id AS SUM_OF_NUMS
FROM book AS b;

----------------------------------------------------------------

SELECT *
FROM BOOK
         JOIN BOOK_AUTHOR BA on book.ID = BA.book_id
         JOIN author a on a.id = BA.author_id
WHERE book_id = 3;


