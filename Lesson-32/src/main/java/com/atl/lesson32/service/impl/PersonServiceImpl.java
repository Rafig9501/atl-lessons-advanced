package com.atl.lesson32.service.impl;

import com.atl.lesson32.entity.Person;
import com.atl.lesson32.repository.PersonRepository;
import com.atl.lesson32.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Override
    public ResponseEntity<Person> savePerson(Person person) {
        Person personSaved = personRepository.savePerson(person);
        return new ResponseEntity<>(personSaved, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Person> getPersonsById(Integer id) {
        Person person = personRepository.getPerson(id);
        return ResponseEntity.status(HttpStatus.OK).body(person);
    }

    @Override
    public ResponseEntity<String> deletePerson(Integer id) {
        Person personFromDb = personRepository.getPerson(id);
        if (personFromDb == null) {
            return new ResponseEntity<>("No person with given id found", HttpStatus.FORBIDDEN);
        }
        int i = personRepository.deletePerson(id);
        return new ResponseEntity<>("DELETED", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Person> updatePerson(Person person) {
        if (person.getId() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        personRepository.updatePerson(person);
        return ResponseEntity.status(HttpStatus.OK).body(person);
    }
}