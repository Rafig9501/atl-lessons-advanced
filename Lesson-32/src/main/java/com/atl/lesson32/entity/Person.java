package com.atl.lesson32.entity;

import lombok.Data;

@Data
public class Person {

    private Integer id;
    private String name;
    private Integer age;
}