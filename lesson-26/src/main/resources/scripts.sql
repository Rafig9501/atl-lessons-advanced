select array_agg(fruit) from basket;

select array_agg(id || ' - ' || fruit order by id desc) from basket;

select avg(age) from company;

select * from basket where (select max(age) > 0 from company);

select * from company where age in (select count from users where count > 0 and count < 30);

insert into basket(fruit) values(encode('test', 'base64'));
select decode(fruit, 'base64') from basket where id = 29;

select current_date;


select to_char(current_date, 'YYYY-MM-DD');

select to_number('123', '9');

select cast('123' AS INTEGER);

SELECT current_date - '2022-08-08' :: date difference;

SELECT round(avg(age)) FROM company;

SELECT avg(age) FROM company;

SELECT count(*) from company;

SELECT max(age) from company;

SELECT name, min(age) from company group by name having name = 'kate';

ALTER TABLE company add column surname varchar;

SELECT surname, max(age) from company group by surname having surname = 'Johns';

select avg(age) from company;

select age from company;

select * from company where age > (select avg(age) from company);

select id, age, (select min(age) from company) from company c;

select * from (SELECT count(count)  from users), company;

UPDATE company set age = age + company.id where age in
                                                (select age from company where age > 30);

DELETE FROM company where age in (select age from company where age > 30);

SELECT * FROM company where age in (select age from company where age > 30);

SELECT * FROM company where age not in (select age from company where age > 30);

SELECT age FROM company where exists (select count from users);

select age from company where age > 30;

select age
from company where age > any (select count from users where id = 6 or id = 7);

select count from users where id = 6 or id = 7;

select age from company where age > some (select count from users where id = 6 or id = 7);