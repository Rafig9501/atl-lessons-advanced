package com.atl.util;

public class StringPrettier {

    private boolean checkIsStringNull;

    public void setCheckIsStringNull(boolean checkIsStringNull) {
        this.checkIsStringNull = checkIsStringNull;
    }

    public String prettyString(String string) {
        if (!checkIsStringNull) {
            return string.trim();
        } else {
            if (string != null) {
                return string.trim();
            }
            return string;
        }
    }
}
