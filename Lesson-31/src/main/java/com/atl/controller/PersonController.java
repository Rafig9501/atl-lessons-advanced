package com.atl.controller;

import com.atl.entity.Person;
import com.atl.service.PersonService;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<Person> createPerson(@RequestBody Person person) {
        return personService.savePerson(person);
    }

    @GetMapping(value = "/get")
    private ResponseEntity<List<Person>> getByName(@RequestParam String personName) {
        return personService.getPersonsByName(personName);
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<String> deletePerson(@RequestBody Person person) {
        return personService.deletePerson(person);
    }

    @PutMapping(value = "/update")
    private ResponseEntity<Person> updatePerson(@RequestBody Person person) {
        return personService.updatePerson(person);
    }
}
