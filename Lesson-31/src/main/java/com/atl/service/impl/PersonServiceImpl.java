package com.atl.service.impl;

import com.atl.entity.Person;
import com.atl.service.PersonService;
import com.atl.util.StringPrettier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonServiceImpl implements PersonService {

    @Value("${application.admin_name}")
    private String adminName;

    private final StringPrettier stringPrettier;

    private final ArrayList<Person> people = new ArrayList<>();

    public PersonServiceImpl(StringPrettier stringPrettier) {
        this.stringPrettier = stringPrettier;
    }

    @Override
    public ResponseEntity<Person> savePerson(Person person) {
        people.add(person);
        //        System.out.println("adminName " + adminName);
        return new ResponseEntity<>(person, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Person>> getPersonsByName(String personName) {
        List<Person> filteredPeople = people.stream().filter(p -> p.getName().equals(personName)).toList();
        if (filteredPeople.isEmpty()) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(filteredPeople, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deletePerson(Person person) {
        if (person.getName() == null || person.getLastName() == null || person.getAge() == null) {
            return new ResponseEntity<>("Provide all properties of Person", HttpStatus.BAD_REQUEST);
        }
        Optional<Person> optionalPerson =
            people.stream().filter(p -> p.getAge().equals(person.getAge()) && p.getName().equals(person.getName()) &&
                p.getLastName().equals(person.getLastName())).findFirst();
        if (optionalPerson.isEmpty()) {
            return new ResponseEntity<>("No person with given properties", HttpStatus.NOT_FOUND);
        }
        people.remove(optionalPerson.get());
        return new ResponseEntity<>("DELETED", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Person> updatePerson(Person person) {
        if (person.getName() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Optional<Person> optionalPerson =
            people.stream().filter(p -> p.getName().equals(person.getName())).findFirst();
        if (optionalPerson.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Person personFromDb = optionalPerson.get();
        people.remove(personFromDb);
        if (person.getLastName() != null) {
            personFromDb.setLastName(person.getLastName());
        }
        if (person.getAge() != null) {
            personFromDb.setAge(person.getAge());
        }
        people.add(personFromDb);
        return ResponseEntity.status(HttpStatus.OK).body(personFromDb);
    }
}