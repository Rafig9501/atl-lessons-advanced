package com.atl.service;

import com.atl.entity.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonService {

    ResponseEntity<Person> savePerson(Person person);

    ResponseEntity<List<Person>> getPersonsByName(String personName);

    ResponseEntity<String> deletePerson(Person person);

    ResponseEntity<Person> updatePerson(Person person);
}
