package com.atl.entity;

import lombok.Data;

@Data
public class Person {

    private String name;
    private String lastName;
    private String age;
}
