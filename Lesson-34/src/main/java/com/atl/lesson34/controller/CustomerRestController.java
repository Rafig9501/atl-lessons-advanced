package com.atl.lesson34.controller;

import com.atl.lesson34.entity.Customer;
import com.atl.lesson34.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerRestController {

    private final CustomerService customerService;

    @PostMapping("/create")
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        return customerService.createCustomer(customer);
    }

    @GetMapping("/get")
    public ResponseEntity<Customer> getCustomer(@RequestParam Long id) {
        return customerService.getCustomer(id);
    }

    @PostMapping("/upload")
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            InputStream inputStream = file.getInputStream();

            System.out.println("Uploaded the file successfully: " + file.getOriginalFilename());
            return ResponseEntity.status(HttpStatus.OK).body(new Object());
        } catch (Exception e) {
            System.out.println(
                "Could not upload the file: " + file.getOriginalFilename() + ". Error: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new Object());
        }
    }
}