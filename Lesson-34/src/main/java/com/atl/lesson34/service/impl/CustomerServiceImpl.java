package com.atl.lesson34.service.impl;

import com.atl.lesson34.entity.Customer;
import com.atl.lesson34.exception.*;
import com.atl.lesson34.repository.CustomerRepository;
import com.atl.lesson34.service.CustomerService;
import lombok.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    @SneakyThrows
    public ResponseEntity<Customer> createCustomer(Customer customer) {
//        Optional<Customer> customerFromDb = customerRepository.findById(customer.getId());
//        if (customerFromDb.isPresent()) {
//            throw new CustomerExistsException("Customer with id " + customer.getId() + " already exists");
//        }
        customerRepository.save(customer);
        return ResponseEntity.status(200).body(customer);
    }

    @Override
    @SneakyThrows
    public ResponseEntity<Customer> getCustomer(Long id) {
        Customer customer =
            customerRepository.findById(id).stream().filter(c -> c.getId().equals(id)).findFirst().orElseThrow(() ->
                new NoSuchCustomerException("Customer with id " + id + "does not exist"));
        return ResponseEntity.status(200).body(customer);
    }
}
