package com.atl.lesson34.service;

import com.atl.lesson34.entity.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {
    ResponseEntity<Customer> createCustomer(Customer customer);

    ResponseEntity<Customer> getCustomer(Long id);
}
