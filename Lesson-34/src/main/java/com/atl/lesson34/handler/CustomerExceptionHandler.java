package com.atl.lesson34.handler;

import com.atl.lesson34.entity.ErrorResponse;
import com.atl.lesson34.exception.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.*;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
@RequiredArgsConstructor
@Log4j2
public class CustomerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleExceptions(Exception ex, WebRequest request) {
        return buildErrorResponse(ex, request);
    }

    @ExceptionHandler(value = ArrayIndexOutOfBoundsException.class)
    public ResponseEntity<Object> handleArrayOutOfBounds(){
        // your logic
        return null;
    }

    private ResponseEntity<Object> buildErrorResponse(Exception ex, WebRequest request) {
        if (ex instanceof CustomerExistsException) {
            return getResponseEntity(ex, HttpStatus.BAD_REQUEST, request, ex.getMessage());
        } else if (ex instanceof NoSuchCustomerException) {
            return getResponseEntity(ex, HttpStatus.NOT_FOUND, request, ex.getMessage());
        } else return getResponseEntity(ex, HttpStatus.INTERNAL_SERVER_ERROR, request, ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
        HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleMethodArgumentNotValid(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
        HttpStatus status, WebRequest request) {
        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
        HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleHttpRequestMethodNotSupported(ex, headers, status, request);
    }

    private ResponseEntity<Object> getResponseEntity(Exception ex, HttpStatus status, WebRequest request, String message) {
        ErrorResponse errorResponse = ErrorResponse.builder()
            .message(message)
            .timestamp(LocalDateTime.now())
            .statusCode(status.value())
            .build();
        return new ResponseEntity<>(errorResponse, status);
    }
}
