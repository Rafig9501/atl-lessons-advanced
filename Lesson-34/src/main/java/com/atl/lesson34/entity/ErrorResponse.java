package com.atl.lesson34.entity;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class ErrorResponse {

    private Integer statusCode;
    private String message;
    private LocalDateTime timestamp;
}
