package com.atl.lesson34.entity;

import lombok.*;
import org.springframework.stereotype.Service;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Service
@EqualsAndHashCode
@ToString
@Builder
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;
}