package com.atl.lesson34.exception;

public class NoSuchCustomerException extends Exception {

    public NoSuchCustomerException(String message) {
        super(message);
    }
}
