package com.atl.lesson33.controller;

import com.atl.lesson33.entity.Person;
import com.atl.lesson33.service.PersonService;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<Person> createPerson(@RequestBody Person person) {
        return personService.savePerson(person);
    }


    @GetMapping(value = "/get")
    private ResponseEntity<Person> getById(@RequestParam Integer id) {
        return personService.getPersonsById(id);
    }

    @DeleteMapping(value = "/delete")
    private ResponseEntity<String> deletePerson(@RequestParam Integer id) {
        return personService.deletePerson(id);
    }

    @PutMapping(value = "/update")
    private ResponseEntity<Person> updatePerson(@RequestBody Person person) {
        return personService.updatePerson(person);
    }
}
