package com.atl.lesson33.service.impl;

import com.atl.lesson33.entity.Person;
import com.atl.lesson33.repository.PersonRepository;
import com.atl.lesson33.service.PersonService;
import lombok.extern.log4j.Log4j2;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {this.personRepository = personRepository;}

    @Override
    public ResponseEntity<Person> savePerson(Person person) {
        try {
            log.debug("got person to save DEBUG LEVEL");
            log.info("got person to save INFO LEVEL");
            Person personSaved = personRepository.savePerson(person);
            log.warn("person saved to db WARNING LEVEL");
            return new ResponseEntity<>(personSaved, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("error saving person to db - " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Person> getPersonsById(Integer id) {
        Person person = personRepository.getPerson(id);
        return ResponseEntity.status(HttpStatus.OK).body(person);
    }

    @Override
    public ResponseEntity<String> deletePerson(Integer id) {
        Person personFromDb = personRepository.getPerson(id);
        if (personFromDb == null) {
            return new ResponseEntity<>("No person with given id found", HttpStatus.FORBIDDEN);
        }
        int i = personRepository.deletePerson(id);
        return new ResponseEntity<>("DELETED", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Person> updatePerson(Person person) {
        if (person.getId() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        personRepository.updatePerson(person);
        return ResponseEntity.status(HttpStatus.OK).body(person);
    }
}