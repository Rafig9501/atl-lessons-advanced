package com.atl.lesson33.service;

import com.atl.lesson33.entity.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public interface PersonService {

    ResponseEntity<Person> savePerson(Person person);

    ResponseEntity<Person> getPersonsById(Integer id);

    ResponseEntity<String> deletePerson(Integer id);

    ResponseEntity<Person> updatePerson(Person person);
}
