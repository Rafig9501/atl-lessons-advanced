package com.atl.lesson33.repository;

import com.atl.lesson33.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Person savePerson(Person person) {
        jdbcTemplate.execute("INSERT INTO persons (id, name, age) VALUES" + "(" + person.getId() +
            ",'" + person.getName() + "'," + person.getAge() + ")");
        return person;
    }

    public Person getPerson(Integer id) {
        Person person = jdbcTemplate.queryForObject(
            "SELECT * FROM persons WHERE id = ?", BeanPropertyRowMapper.newInstance(Person.class), id);
        return person;
    }

    public int deletePerson(Integer id) {
        return jdbcTemplate.update("DELETE FROM persons WHERE id =?", id);
    }

    public int updatePerson(Person person) {
        int returnInt = jdbcTemplate.update("UPDATE persons SET name =?, age =? WHERE id =?",
            person.getName(), person.getAge(), person.getId());
        return returnInt;
    }
}
