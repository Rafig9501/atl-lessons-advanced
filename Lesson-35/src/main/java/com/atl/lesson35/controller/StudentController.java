package com.atl.lesson35.controller;

import com.atl.lesson35.entity.StudentEntity;
import com.atl.lesson35.service.StudentService;
import lombok.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/student")
@RestController
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public ResponseEntity<List<StudentEntity>> getStudentByNameAndSurname(String name, String surname){
        return studentService.getStudentByStudentNameAndSurname(name, surname);
    }
}
