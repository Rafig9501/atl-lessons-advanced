package com.atl.lesson35.service.impl;

import com.atl.lesson35.entity.StudentEntity;
import com.atl.lesson35.repository.StudentRepository;
import com.atl.lesson35.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public ResponseEntity<List<StudentEntity>> getStudentByStudentNameAndSurname(String name, String surname) {
        List<StudentEntity> studentEntities = studentRepository.findByNameAndSurnameParameters(name, surname);
        return ResponseEntity.ok(studentEntities);
    }
}
