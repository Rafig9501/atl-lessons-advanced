package com.atl.lesson35.service;

import com.atl.lesson35.entity.StudentEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {


    ResponseEntity<List<StudentEntity>> getStudentByStudentNameAndSurname(String name, String surname);
}