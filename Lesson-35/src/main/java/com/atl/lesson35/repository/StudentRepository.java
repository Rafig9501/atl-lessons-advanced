package com.atl.lesson35.repository;

import com.atl.lesson35.entity.StudentEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

    List<StudentEntity> findByNameAndSurname(String name, String surname);

    boolean existsByName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM students WHERE name = :studentName AND surname = :studentSurname")
    List<StudentEntity> findByNameAndSurnameParametersNative(@Param("studentName") String studentName,
        @Param("studentSurname") String studentSurname);

    @Query(value = "select StudentEntity from StudentEntity where name = :studentName and surname = " +
        ":studentSurname")
    List<StudentEntity> findByNameAndSurnameParametersHibernate(@Param("studentName") String studentName,
        @Param("studentSurname") String studentSurname);
}