package com.atl.lesson30.entity;

import lombok.*;

@AllArgsConstructor
@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Student {

    private Integer id;
    private String name;
    private Integer age;
    private String university;
}