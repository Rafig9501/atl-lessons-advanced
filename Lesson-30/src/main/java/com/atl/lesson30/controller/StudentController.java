package com.atl.lesson30.controller;

import com.atl.lesson30.entity.Student;
import com.atl.lesson30.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping(path = "/get-by-id", produces = MediaType.APPLICATION_JSON_VALUE)
//    @RequestMapping(path = "/get-by-id", method = RequestMethod.GET)
    public Student getById(@RequestParam Integer id){
        return studentService.findStudentById(id);
    }
}
