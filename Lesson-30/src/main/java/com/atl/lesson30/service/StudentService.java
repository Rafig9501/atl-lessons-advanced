package com.atl.lesson30.service;

import com.atl.lesson30.entity.Student;
import org.springframework.stereotype.Service;

@Service
public interface StudentService {

    Student findStudentById(Integer id);
}
