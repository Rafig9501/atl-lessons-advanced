package com.atl.lesson30.service.impl;

import com.atl.lesson30.entity.Student;
import com.atl.lesson30.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentServiceImpl implements StudentService {

    private final List<Student> students = Arrays.asList(
        new Student(1, "John", 20, "MIT"),
        new Student(2, "Ann", 19, "Stanford"),
        new Student(3, "Mamed", 21, "ADIU"),
        new Student(4, "Kamran", 22, "ASEU"),
        new Student(5, "Araz", 20, "ADU"));

    @Override
    public Student findStudentById(Integer id) {
        return students.stream().filter(s -> s.getId().equals(id)).findFirst().orElse(new Student());
    }
}
