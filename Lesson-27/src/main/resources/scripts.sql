
DROP TABLE IF EXISTS T1;
CREATE TABLE T1 (label CHAR(1) PRIMARY KEY);

DROP TABLE IF EXISTS T2;
CREATE TABLE T2 (score INT PRIMARY KEY);

INSERT INTO T1 (label)
VALUES
    ('A'),
    ('B');

INSERT INTO T2 (score)
VALUES
    (1),
    (2),
    (3);

select * from T2 cross join T1;

DROP TABLE IF EXISTS categories;
CREATE TABLE categories (
                            category_id serial PRIMARY KEY,
                            category_name VARCHAR (255) NOT NULL
);

DROP TABLE IF EXISTS products;
CREATE TABLE products (
                          product_id serial PRIMARY KEY,
                          product_name VARCHAR (255) NOT NULL,
                          category_id INT NOT NULL,
                          FOREIGN KEY (category_id) REFERENCES categories (category_id)
);


INSERT INTO categories (category_name)
VALUES
    ('Smart Phone'),
    ('Laptop'),
    ('Tablet');

INSERT INTO products (product_name, category_id)
VALUES
    ('iPhone', 1),
    ('Samsung Galaxy', 1),
    ('HP Elite', 2),
    ('Lenovo Thinkpad', 2),
    ('iPad', 3),
    ('Kindle Fire', 3);

SELECT * FROM products NATURAL LEFT JOIN categories;

CREATE TABLE employee (
                          employee_id INT PRIMARY KEY,
                          first_name VARCHAR (255) NOT NULL,
                          last_name VARCHAR (255) NOT NULL,
                          manager_id INT,
                          FOREIGN KEY (manager_id)
                              REFERENCES employee (employee_id)
                              ON DELETE CASCADE
);
INSERT INTO employee (
    employee_id,
    first_name,
    last_name,
    manager_id
)
VALUES
    (1, 'Windy', 'Hays', NULL),
    (2, 'Ava', 'Christensen', 1),
    (3, 'Hassan', 'Conner', 1),
    (4, 'Anna', 'Reeves', 2),
    (5, 'Sau', 'Norman', 2),
    (6, 'Kelsie', 'Hays', 3),
    (7, 'Tory', 'Goff', 3),
    (8, 'Salley', 'Lester', 3);

SELECT
        e.first_name || ' ' || e.last_name employee,
        m .first_name || ' ' || m .last_name manager
FROM
    employee e
        INNER JOIN employee m ON m .employee_id = e.manager_id
ORDER BY manager;

SELECT
        e.first_name || ' ' || e.last_name employee,
        m .first_name || ' ' || m .last_name manager
FROM
    employee e
        LEFT JOIN employee m ON m .employee_id = e.manager_id
ORDER BY manager;

------

create table employees(
                          id bigserial NOT NULL primary key,
                          name varchar(255),
                          address varchar(255)
);

create table employee_contact(
                                 id bigserial NOT NULL primary key,
                                 phone varchar(255),
                                 email varchar(255),
                                 employee_id bigint unique NOT NULL,
                                 constraint fk_employee_id foreign key (employee_id) references employees(id)
);

insert into employees (name, address) values ('John', 'Johnson');
insert into employee_contact (phone, email, employee_id) values ('+994999999999', 'test@example.com', 1);
insert into employee_contact (phone, email, employee_id) values ('+994999555555', 'test5@example.com', 1);

------------------------------------------------------------------------------------------------------------------------

create table courses(
                        id bigserial NOT NULL primary key,
                        name varchar(255),
                        instructor varchar(255)
);

create table students(
                         id bigserial not null primary key,
                         name varchar(255),
                         course_id bigint not null,
                         constraint fk_course_id foreign key (course_id) references courses(id)
);

insert into courses (name, instructor) values ('Jack', 'Adam');
insert into students (name, course_id) values ('Ann', 1);
insert into students (name, course_id) values ('Jonathan', 1);

------------------------------------------------------------------------------------------------------------------------

create table books(
                      id bigserial not null primary key,
                      title varchar(255),
                      author varchar(255)
);

create table genres(
                       id bigserial not null primary key,
                       name  varchar(255)
);

create table book_genre(
                           book_id bigint not null,
                           genre_id bigint not null,
                           constraint fk_book_id foreign key (book_id) references books(id),
                           constraint fk_genre foreign key (genre_id) references genres(id),
                           constraint pk_book_genre primary key (book_id, genre_id)
);

insert into books(title, author) values ('Forest', 'Tomas');
insert into books(title, author) values ('Summer', 'Alex');
insert into books(title, author) values ('Winter', 'Kate');

insert into genres(name) values ('Action');
insert into genres(name) values ('Melodrama');
insert into genres(name) values ('Fantasy');

insert into book_genre(book_id, genre_id) values (1, 2);
insert into book_genre(book_id, genre_id) values (2, 1);
insert into book_genre(book_id, genre_id) values (3, 3);

------------------------------------------------------------------------------------------------------------------------

select books.id, books.author, books.title, genres.name from books
                                                                 left join book_genre bg on books.id = bg.book_id
                                                                 left join genres on genres.id = bg.genre_id;

------------------------------------------------------------------------------------------------------------------------

select * from students inner join courses c on students.id = c.id;

------------------------------------------------------------------------------------------------------------------------

select books.id, books.author, books.title, genres.name from books
                                                                 left join book_genre bg on books.id = bg.book_id
                                                                 left join genres on genres.id = bg.genre_id;

select * from courses left join students on students.id = courses.id;

------------------------------------------------------------------------------------------------------------------------

select * from students right join courses on courses.id = students.id left join employees on employees.id = courses.id;

------------------------------------------------------------------------------------------------------------------------

select * from  students full join courses on students.id = courses.id;

------------------------------------------------------------------------------------------------------------------------

select * from courses cross join students;

select * from courses, students;

------------------------------------------------------------------------------------------------------------------------

select * from students natural join courses;

create table categories(
                           id bigserial primary key,
                           name varchar(255) not null
);

create table product (
                         id bigserial primary key,
                         name varchar(255),
                         category_id int not null,
                         constraint fk_category_id foreign key(category_id) references categories(id)
);

insert into categories(name) values ('Movie'), ('Phone'), ('Laptop');

insert into product(name, category_id) VALUES ('Movie', 1),   ('Sparrow', 2), ('HP', 3);

select * from categories natural join product;


------------------------------------------------------------------------------------------------------------------------

create table workers (
                         id bigserial primary key,
                         first_name varchar(255),
                         last_name varchar(255),
                         manager_id bigint,
                         constraint fk_manager_id foreign key (manager_id) references workers(id) on delete cascade
);

insert into workers(
    first_name, last_name
) values ('Windy', 'Johns'), ('Amy', 'Norman'), ('Johns', 'Johns'), ('Tom', 'Hanks'), ('Kate', 'Anderson');

select * from workers inner join workers w1 on w1.id = workers.manager_id order by workers.manager_id;