package com.atl.tech.controller;

import com.atl.tech.model.Person;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PersonController {

    @GetMapping("/person")
    public ResponseEntity<Person> getPerson(){
        Person person = new Person("John", 21);
        return new ResponseEntity<>(person, HttpStatus.CREATED);
    }
}
