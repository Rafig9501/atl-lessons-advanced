CREATE TABLE IF NOT EXISTS users
(
    id    int not null primary key,
    count double precision
);


CREATE TABLE IF NOT EXISTS COMPANY
(
    ID   int not null primary key,
    NAME varchar(255),
    AGE  int not null
    );

INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (1, null, 82);


SELECT NAME C_N
FROM COMPANY
WHERE NAME is not null;

SELECT concat('name', 'surname') concat_name;

INSERT INTO users (id, count)
VALUES (1, 3);

SELECT count ^ 9
FROM users;

SELECT |/ count
from users;

SELECT count >> 2
FROM users;

SELECT abs(count)
from users;

select ceil(count)
from users;

select factorial(5) as count;

SELECT count
from users
where count > 0
  and users.id > 37;


------------------------------------------------------------------------------------
INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (2, 'Jack', 28);
INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (3, 'Ann', 29);
INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (4, 'Jack', 30);
INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (5, 'Ann', 31);
INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (6, 'Jon', 32);
INSERT INTO COMPANY(ID, NAME, AGE)
VALUES (7, 'Kate', 33);

SELECT ID, NAME
FROM COMPANY;

SELECT NAME, count(NAME) name_repetition
FROM COMPANY
GROUP BY NAME
ORDER BY name_repetition DESC;

SELECT NAME, count(NAME) name_repetition
FROM COMPANY
GROUP BY NAME
HAVING (count(NAME) >= 2)
ORDER BY name_repetition;

------------------------------------------------------------------------------------

CREATE TABLE BASKET
(
    ID    SERIAL PRIMARY KEY,
    FRUIT VARCHAR(50) NOT NULL
);

INSERT INTO BASKET(FRUIT)
VALUES ('apple');
INSERT INTO BASKET(FRUIT)
VALUES ('apple');
INSERT INTO BASKET(FRUIT)
VALUES ('orange');
INSERT INTO BASKET(FRUIT)
VALUES ('orange');
INSERT INTO BASKET(FRUIT)
VALUES ('orange');
INSERT INTO BASKET(FRUIT)
VALUES ('orange');
INSERT INTO BASKET(FRUIT)
VALUES ('apple');
INSERT INTO BASKET(FRUIT)
VALUES ('banana');
INSERT INTO BASKET(FRUIT)
VALUES ('apple');

----------------------------------------------------------------------------------

DELETE
FROM BASKET a USING BASKET b
where a.ID > b.ID
  and a.FRUIT = b.FRUIT;

DELETE
FROM BASKET
WHERE ID IN
      (SELECT ID
       FROM (SELECT ID,
                    row_number() over (PARTITION BY FRUIT ORDER BY ID) ROW_NUM
             FROM BASKET) B
       WHERE B.ROW_NUM > 1);

SELECT *
FROM BASKET
         LIMIT 7;

SELECT *
FROM BASKET
ORDER BY ID DESC
    LIMIT 4;

SELECT *
FROM BASKET
ORDER BY ID DESC
    LIMIT 4 OFFSET 2;

--------------------------------------------------------------------------------

CREATE TABLE if not exists DOCUMENTS
(
    ID       SERIAL PRIMARY KEY,
    DOC_DATE date        NOT NULL DEFAULT CURRENT_DATE,
    DOC_NAME varchar(80) NOT NULL
    );

INSERT INTO DOCUMENTS(DOC_NAME)
VALUES ('doc1');
INSERT INTO DOCUMENTS(DOC_NAME)
VALUES ('doc2');
INSERT INTO DOCUMENTS(DOC_DATE, DOC_NAME)
VALUES ('2022-12-12', 'doc3');
INSERT INTO DOCUMENTS(DOC_DATE, DOC_NAME)
VALUES ('2022-11-12', 'doc4');
INSERT INTO DOCUMENTS(DOC_DATE, DOC_NAME)
VALUES ('2022-10-12', 'doc5');
INSERT INTO DOCUMENTS(DOC_NAME)
VALUES ('doc4');
INSERT INTO DOCUMENTS(DOC_DATE, DOC_NAME)
VALUES ('2024-12-12', 'doc4');

select now() :: timestamp;

SELECT TO_CHAR(now() :: timestamp, 'dd-MM-yyyy HH:mm:ss');

SELECT now() - DOC_DATE
FROM DOCUMENTS;

--------------------------------------------------------------------------------

CREATE TABLE PERIODS
(
    ID         SERIAL PRIMARY KEY,
    START_DATE date NOT NULL,
    END_DATE   date NOT NULL
);

INSERT INTO PERIODS(START_DATE, END_DATE)
VALUES ('2021-01-01', '2021-08-09');
INSERT INTO PERIODS(START_DATE, END_DATE)
VALUES ('2022-01-01', '2022-08-09');
INSERT INTO PERIODS(START_DATE, END_DATE)
VALUES ('2022-03-01', '2022-05-09');
INSERT INTO PERIODS(START_DATE, END_DATE)
VALUES ('2023-01-07', '2023-02-09');
INSERT INTO PERIODS(START_DATE, END_DATE)
VALUES ('2023-02-05', '2023-05-09');

SELECT ID, PERIODS.START_DATE, PERIODS.END_DATE, END_DATE - PERIODS.START_DATE AS DIFFERENCE
FROM PERIODS;

select age(START_DATE)
from PERIODS;

select extract(month from START_DATE)
from PERIODS;

SELECT *
FROM COMPANY
WHERE AGE NOT BETWEEN 20 AND 30;

select *
FROM COMPANY
WHERE AGE IN (20, 30, 28, 29);

select *
FROM COMPANY
WHERE AGE not IN (20, 30, 28, 29);

select *
from COMPANY
where NAME like 'K%';

SELECT *
FROM COMPANY
WHERE NAME ilike '%a_';
SELECT *
FROM COMPANY
WHERE NAME not ilike '%a_';

------------------------------------------------------------------------------------

ALTER TABLE users
    ADD COLUMN COUNT2 int;

INSERT INTO users (ID, COUNT, COUNT2)
VALUES (2, 8, 6);
INSERT INTO users (ID, COUNT, COUNT2)
VALUES (3, 7, 3);
INSERT INTO users (ID, COUNT, COUNT2)
VALUES (4, 4, 2);
INSERT INTO users (ID, COUNT, COUNT2)
VALUES (5, 6, 1);
INSERT INTO users (ID, COUNT, COUNT2)
VALUES (6, 5, 10);
INSERT INTO users (ID, COUNT, COUNT2)
VALUES (7, 4, 3);

SELECT count, users.COUNT2
FROM users
ORDER BY count, COUNT2;

SELECT ID, NAME, length(NAME) name_length
from company
order by name_length;

SELECT GREATEST('gt', 'tygh', 'ftfxf', 'cvcb');

SELECT NULLIF(5, 7);

SELECT NULLIF(5, 5);

SELECT COALESCE(NULL, 6, NULL, 7);

SELECT CASE
           WHEN 1 > 3 THEN 'Greater'
           WHEN 1 > 3 THEN 'Greater'
           WHEN 1 > 3 THEN 'Greater'
           WHEN 0 > 3 THEN 'Greater'
           ELSE 'Lesser' END;

----------------------------------------------------------------

CREATE TABLE students (
                          ID SERIAL PRIMARY KEY,
                          NAME VARCHAR(255) NOT NULL,
                          SURNAME VARCHAR(255));

insert into students(name, surname) VALUES ('John', 'Johny');
insert into students(name, surname) VALUES ('Ann', 'Peterson');
insert into students(name, surname) VALUES ('Sara', 'Tomas');

select decode(NAME, 'base64') from students;