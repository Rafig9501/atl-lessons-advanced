package org.atl_tech;


import java.io.*;
import java.sql.*;

public class Main {

    static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    static final String USER = "myusername";
    static final String PASS = "mypassword";
    static final String QUERY = "SELECT * FROM products";

    public static void main(String[] args) throws IOException {
        selectProduct();
        updateProduct();
        selectProduct();
    }

    private static void selectProduct() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(QUERY)) {
            while (rs.next()) {
//                rs.first();
//                rs.last();
//                int row = rs.getRow();
//                rs.absolute(3);
//                rs.relative(2);
                System.out.print("ID: " + rs.getInt("product_id"));
                System.out.print(", product name: " + rs.getString("product_name"));
                System.out.print(", category id: " + rs.getInt("category_id"));
                System.out.print("\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateProduct() {
        String updatePositionSql = "UPDATE products SET product_name=? WHERE product_id=?";
        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement prepareStatement = connection.prepareStatement(updatePositionSql)) {
            connection.setAutoCommit(false);
            prepareStatement.setString(1, "One Plus");
            prepareStatement.setInt(2, 4);
            prepareStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}